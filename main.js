


let age = prompt("Будь ласка, введіть свій вік:");
age = parseInt(age);

while (isNaN(age)) {
    age = Number(prompt("Будь ласка, вкажіть свій вік числом"));
}

if (age < 12) {
alert("ви є дитиною");
    
} 
else if (age < 18) {
    alert("Ви є підлітком."); 
}
else {
    alert("Ви є дорослим.");
}


const daysInMonth = {
    'січень': 31,
    'лютий': 28,
    'березень': 31,
    'квітень': 30,
    'травень': 31,
    'червень': 30,
    'липень': 31,
    'серпень': 31,
    'вересень': 30,
    'жовтень': 31,
    'листопад': 30,
    'грудень': 31
};
let month = prompt("Будь ласка, введіть місяць року українською мовою (наприклад, 'січень'):").toLowerCase();

if (daysInMonth.hasOwnProperty(month)) {
    
    console.log(`У місяці ${month} ${daysInMonth[month]} днів.`);
} else {
    console.log("Введений місяць не знайдено у списку.");
}
